/** 
 *   (c) danimoth, 2011 
 * 
 *   This file is part of Sbonk.
 *
 *   Sbonk is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Sbonk is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Sbonk.  If not, see <http://www.gnu.org/licenses/>.
 */
package i2p.sbonk;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import net.i2p.data.DataFormatException;

/** Container
 * 
 * <p>
 * A Post contains a plain text (that'll be sbonk'd) and a hash referring to a parent post, 
 * in this way is possible to build a tree of posts. 
 * 
 * Every Post have a hash associated with it, which is the SHA-256 of the text concatenated with
 * the parent's hash.
 * 
 * @author danimoth
 *
 */
public class Post  {
	
	/** The text of this post */
	private String text;
	
	/** Separator used when writing/reading the text and the
	 * parent hash.
	 * <p>
	 * The value is ASCII: so 0 is the '\0'.
	 */
	private int separator = 0;
	
	public Post() {
		text = "";
	}
	
	public Post(java.io.InputStream in) throws DataFormatException, IOException {
		populateFrom(in);
	}
	
	/** Get the text of this post
	 * 
	 * @return The post's text
	 */
	public String getText() {
		return text;
	}

	/** Set the text of this post
	 * 
	 * @param text Post text
	 */
	public void setText(String text) {
		this.text = text;
	}

	/** Populate the Post with values from the input stream
	 * <p>
	 * This is complementary to the saveTo method. It reads the text, then the
	 * separator, and then the parent's hash.
	 * 
	 * @param in Input stream
	 * @throws IOException
	 * @throws DataFormatException
	 */
	public void populateFrom(java.io.InputStream in) throws IOException, DataFormatException {
		int value;
		boolean formatOk = false;
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		String message = "";
		
		while ((value = in.read()) != -1) {
			if (value == separator) {
				message = out.toString();
				out.reset();
				formatOk = true;
				continue;
			}
			out.write(value);
		}
		
		if (!formatOk) {
			DataFormatException e = new DataFormatException("Missing separator");
			throw e;
		}
		
		text = message;
	}
	
	public void populateFrom(String value){
		text = value;
	}
	
	/** Save the post in the given output stream
	 * <p>
	 * 
	 * The format is quite simple. Just write the text in plain ("no need to hide something to others"),
	 * followed by a separator, and then the parent's hash.
	 * 
	 * @param out Output stream where the Post'll be written
	 * @throws IOException
	 */
	public void saveTo(java.io.OutputStream out) throws IOException {
		out.write(text.getBytes());
		out.write(separator);
		out.flush();
	}
	
	/** Return the hash of the post
	 * <p>
	 * The hash is built with the MD5 function which operates on the text and on
	 * the parent's hash.
	 * 
	 * @return Post's hash
	 */
	public String getHash() {
		MessageDigest m = null;
		try {
			m = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		m.reset();
		m.update(text.getBytes());
		byte[] digest = m.digest();
		BigInteger bigInt = new BigInteger(1,digest);
		String hashtext = bigInt.toString(16);
		// Now we need to zero pad it if you actually want the full 32 chars.
		while(hashtext.length() < 32 ){
		  hashtext = "0"+hashtext;
		}

		return hashtext;
	}
}
