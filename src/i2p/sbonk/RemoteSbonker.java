package i2p.sbonk;

import i2p.sbonk.net.I2PNetwork;

import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.logging.Logger;

import net.i2p.I2PException;
import net.i2p.data.DataFormatException;
import net.i2p.data.Destination;

/** Remote sbonker class. It represent a remote sbonker.
 * 
 * @author danimoth
 *
 */
public class RemoteSbonker extends Sbonker {

	private static Logger logger = Logger.getLogger("RemoteSbonker");
	
	/** Construct a sbonker from an input stream (a previous saved sbonker)
	 * 
	 * @param stream Input stream
	 * @throws DataFormatException
	 * @throws IOException
	 */
	public RemoteSbonker(InputStream stream) throws DataFormatException, IOException {
		readFrom(stream);
	}
	
	/** Construct a sbonker from its netname, performing a lookup into the resolution
	 * table.
	 * 
	 * @param netName The netname of the sbonker
	 * @throws IOException
	 * @throws I2PException
	 */
	public RemoteSbonker(String netName) throws IOException, I2PException {
		String[] values = netName.split(":");
		if (values.length != 2) {
			throw new IOException("Net name isn't valid");
		}
		
		name = values[0];
		logger.info("Remote sbonker added with name " + name + " and destination hash " + values[1]);
		
		I2PNetwork net = I2PNetwork.getInstance();
		try {
			net.resolvFromName(netName);
		} catch (I2PException e) {
			logger.warning(netName + " address can't be resolved into DHT");
			throw e;
		}
	}
	
	@Override
	public int getPostCount() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public LinkedList<Post> getPosts() {
		// TODO Auto-generated method stub
		return null;
	}

}
