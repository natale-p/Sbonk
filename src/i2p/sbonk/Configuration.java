package i2p.sbonk;

import net.i2p.I2PAppContext;

/** Configuration class for Sbonk.
 * 
 * It contains some configuration parameters.
 * 
 * @author danimoth
 *
 */
public class Configuration {

	/** Path of the plugin */
	public static String pluginPath = I2PAppContext.getGlobalContext().getAppDir().getAbsolutePath() 
			+ "/plugins/sbonk/";
	
	/** Path of the database files */
	public static final String databasePath = pluginPath + "db/";
	
	/** Path of network files */
	public static final String networkPath = pluginPath + "net/";
	
	/** Path of the bootstrap nodes text file */
	public static final String bootstrapNodeFile = networkPath + "default_bootstrap.txt";
}
