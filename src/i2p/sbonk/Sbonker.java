package i2p.sbonk;

import i2p.sbonk.db.TextDatabase;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.LinkedList;

import net.i2p.data.DataFormatException;
import net.i2p.data.Destination;

/** Class which represents authors of posts.
 * 
 * It contains some general function, which apply both to LocalSbonker and RemoteSbonker,
 * the two implementing class.
 * 
 * @author danimoth
 *
 */
public abstract class Sbonker {

	/** Destination of the sbonker */
	protected Destination destination;
	
	/** Name of the sbonker */
	protected String name = "";
	
	/** Separator used when writing/reading from a stream.
	 * <p>
	 * The value is ASCII: so 0 is the '\0'.
	 */
	private int separator = 0;
	
	/** Public constructor: it does nothing.
	 * 
	 * Remember to set destination and name before using it.
	 */
	public Sbonker() {
	}
	
	/** Return the destination of the Sbonker
	 * 
	 * @return The destination of the Sbonker
	 */
	public Destination getDestination() {
		return destination;
	}

	/** Get the Netname of the sbonker
	 * 
	 * @return Netname (in form of name:4digits)
	 */
	public String getNetName() {
		return getName() + ":" + getDestination().getHash().toBase64().substring(0, 4);
	}
	
	/** Return the name of the Sbonker
	 * 
	 * @return name of the sbonker
	 */
	public String getName() {
		return name;
	}
	
	/** Get the post count for the Sbonker
	 * 
	 * @return Count of the post of the Sbonker
	 */
	public abstract int getPostCount();

	/** Get all posts from the Sbonker
	 * 
	 * @return The post list
	 */
	public abstract LinkedList<Post> getPosts();
	
	/** Read the Sbonker from an input stream.
	 * 
	 * @param stream Input stream
	 * @throws IOException
	 * @throws DataFormatException
	 */
	protected void readFrom(InputStream stream) throws IOException, DataFormatException {
		name = new String();
		destination = new Destination();
		
		int value;
		boolean formatOk = false;
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		
		while ((value = stream.read()) != -1) {
			if (value == separator) {
				name = out.toString();
				
				out.reset();
				formatOk = true;
				break;
			}
			
			out.write(value);
		}
		
		while ((value = stream.read()) != -1) {
			out.write(value);
		}
		
		destination.fromBase64(out.toString());
		
		if (formatOk == false) {
			DataFormatException e = new DataFormatException("Missing separator");
			throw e;
		}
	}
	
	/** Save the sbonker persistently on the filesystem.
	 * 
	 * In actual implementations, it saves into a TextDabase.
	 * 
	 * @throws IOException
	 */
	public void savePersistently() throws IOException {
		TextDatabase db = TextDatabase.getInstance();
		OutputStream out = db.getOutputStreamForSbonker(this);
		saveTo(out);
	}
	
	/** Save the sbonker into an OutputStream
	 * 
	 * @param stream OutputStream where save the Sbonker. The stream will be flushed
	 * at the end.
	 * @throws IOException
	 */
	protected void saveTo(OutputStream stream) throws IOException {
		stream.write(name.getBytes());
		stream.write((byte)separator);
		stream.write(destination.toBase64().getBytes());
		stream.flush();
	}
}
