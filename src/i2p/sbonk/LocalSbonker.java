package i2p.sbonk;

import i2p.sbonk.db.TextDatabase;
import i2p.sbonk.net.I2PNetwork;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.LinkedList;
import java.util.Properties;
import java.util.logging.Logger;

import net.i2p.I2PException;
import net.i2p.client.I2PClient;
import net.i2p.client.I2PClientFactory;
import net.i2p.client.I2PSession;
import net.i2p.client.I2PSessionException;
import net.i2p.data.Destination;

/** Class which represent the actual user of Sbonk.
 * 
 * A LocalSbonker could add posts into its personal database, and it is represented
 * remotely with the class RemoteSbonker.
 * 
 * @author danimoth
 *
 */
public class LocalSbonker extends Sbonker {

	private static LocalSbonker instance = null;
	private static Logger logger = Logger.getLogger("LocalSbonker");
	
	/** Get an instance of LocalSbonker
	 * 
	 * @return the LocalSbonker instance, or null when an exception is caught (view log)
	 */
	public static LocalSbonker getInstance() {
	    if(instance == null) {
	        try {
				instance = new LocalSbonker();
			} catch (Exception e) {
				logger.severe(e.getMessage());
			}
	    }
	    
	    return instance;
	}
	
	/** Create a new object
	 * 
	 * It tries to read private key pair from a file, and create a destination
	 * with it (which will be the client's destination).
	 * 
	 * It initializes also the I2PNetwork class
	 * 
	 * @see I2PNetwork
	 * 
	 * @throws IOException
	 * @throws I2PException
	 */
	protected LocalSbonker() throws IOException, I2PException {
		File settings = new File(Configuration.networkPath + "priv.key");
		
		I2PClient client = I2PClientFactory.createClient();
		
		if (!settings.exists()) {
			logger.info("Creating a new public/private key pair for making a Destination");
			try {
				settings.createNewFile();
			} catch (IOException e) {
				throw new IOException("Can't write public/private key pair to " + Configuration.networkPath + "priv.key");
			}
			
			FileOutputStream out = null;
			
			try {
				out = new FileOutputStream(settings);
			} catch (FileNotFoundException e1) {
				throw new IOException("Something strange happened when writing to " + Configuration.networkPath + "priv.key");
			}
			
			try {
				client.createDestination(out);
			} catch (I2PException e) {
				throw new I2PException("Can't create the I2P destination for I2P reasons");
			} catch (IOException e) {
				throw new IOException("Can't create the I2P destination for I/O reasons");
			}
			try {
				out.close();
			} catch (IOException e) {
				throw new IOException("Can't close "+ Configuration.networkPath + "priv.key");
			}
		}
		
		FileInputStream in = null;
		
		try {
			in = new FileInputStream(settings);
		} catch (FileNotFoundException e) {
			throw new IOException("Something strange happened when reading from " + Configuration.networkPath + "priv.key");
		}
		
		Properties prop = new Properties();
		I2PSession session = null;
		try {
			session = client.createSession(in, prop);
		} catch (I2PSessionException e) {
			throw new I2PSessionException("Something strange happened when creating the session from a key pair stored locally");
		}
		
		try {
			in.close();
		} catch (IOException e) {
			throw new IOException("Can't close "+ Configuration.networkPath + "priv.key");
		}
		
		I2PNetwork net = I2PNetwork.getInstance();
		net.initialize(session);
		
		destination = session.getMyDestination();
	}
	
	/** Add a post into database
	 * 
	 * @param post Post to add
	 * @throws IOException
	 */
	public void addPost(Post post) throws IOException {
		TextDatabase db = TextDatabase.getInstance();
		OutputStream out = db.getOutputStreamForPost(this, post);
		
		post.saveTo(out);
	}
	
	@Override
	public Destination getDestination() {
		I2PNetwork net = I2PNetwork.getInstance();
		return net.getDestination();
	}

	@Override
	public String getName() {
		if (name != "")
			return name;

		FileInputStream fstream;
		try {
			fstream = new FileInputStream(Configuration.networkPath + "name.dat");
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			name = br.readLine();
		} catch (FileNotFoundException e) {
			name = "";
		} catch (IOException e) {
			name = "";
		}
		
		return name;
	}

	@Override
	public int getPostCount() {
		TextDatabase db = TextDatabase.getInstance();
		return db.getPostCount(this);
	}

	@Override
	public LinkedList<Post> getPosts() {
		TextDatabase db = TextDatabase.getInstance();
		return db.getAllPosts(this);
	}

	/** Set the name of the local sbonker.
	 * 
	 * It could change, but the netname will also change.
	 * 
	 * @param name The name of the local sbonker
	 * @throws IOException
	 */
	public void setName(String name) throws IOException {
		File nameFile = new File(Configuration.networkPath + "name.dat");
		nameFile.createNewFile();
		
		FileWriter fstream;
		fstream = new FileWriter(nameFile);

		BufferedWriter out = new BufferedWriter(fstream);
		out.write(name);
		out.close();
		this.name = name;
	}

}
