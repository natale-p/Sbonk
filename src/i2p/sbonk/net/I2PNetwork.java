package i2p.sbonk.net;

import i2p.sbonk.Configuration;
import i2p.sbonk.kad.Identifier;
import i2p.sbonk.kad.Kademlia;
import i2p.sbonk.kad.TimestampedValue;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.logging.Logger;


import net.i2p.I2PException;
import net.i2p.client.I2PSession;
import net.i2p.client.I2PSessionException;
import net.i2p.client.datagram.I2PDatagramMaker;
import net.i2p.data.Destination;
/** Implements a network for Sbonk
 * @author danimoth
 *
 */
public class I2PNetwork {
	
	private static Logger logger = Logger.getLogger("I2PNetwork");
	private Kademlia kad;
	private I2PSession session;
	public boolean connected = false;
	
	private static I2PNetwork instance = null;
	
	protected I2PNetwork() throws IOException, I2PSessionException {
		
		logger.info("Creation of the network object on the path " + Configuration.networkPath);
		
		File dir = new File(Configuration.networkPath);
		
		if (!dir.exists()) {
			dir.mkdir();
		} else if (!dir.isDirectory()) {
			IOException e = new IOException(Configuration.networkPath + " isn't a directory");
			throw e;
		}
		
		if (!dir.canRead()) {
			IOException e = new IOException("You have not reading permission on " + Configuration.networkPath);
			throw e;
		}
		
		if (!dir.canWrite()) {
			IOException e = new IOException("You have not writing permission on " + Configuration.networkPath);
			throw e;
		}
		
		File bootstrapNode = new File(Configuration.bootstrapNodeFile);
		
		if (! bootstrapNode.exists()) {
			try {
				bootstrapNode.createNewFile();
				FileWriter fstream;
				fstream = new FileWriter(bootstrapNode);

				BufferedWriter bootstrapFile = new BufferedWriter(fstream);
				bootstrapFile.write("PBEJZD3bsaJBBlMotk41vYY2UlP5Co48GUWI0lAPDK3pnV8S25ziyZhXlL8TeqPpIEbswM6g3zZh2y5Q15RFbDuPgZnglhO1O69PQ9zjtIKTMPtQkIZFjQY9xXjOkVLph3-Fd18HdefmIybC9B6wMm1VZ2tKqX9Ze2ZZKkcXie8KsiACjV7Pq7Q7Eahb1pDIXkBPsaihLdRIKBDhIx8bLzIhM3t-UPfF29jAE0eXgckcvx8SqgEj~REWG2rKcMCX4EzPevLIaeFvrXqYYsck1l7UFpBjsTCDlfcbed326QERgQa2dHZ2JamlV1Ihf5PtLxer4rTAtZu-JbudQxtu~RwG7UH9VEv~fMDP70mfAQUrShY0sHNnzcm97EB8~auIoYyAtSDwI~LwRR4QNAcjdZpp~HzCdPGFjNJUwybQ8uJJfcfaABCYIV6rAT7jp6t~AJQ4JRNUnmqyXW6oIQAjCrWp2Ysp~RxvxKTqUzL5-g4Cz1IB2cdN57LsBQM9xc6HAAAA");
				bootstrapFile.close();
			} catch (IOException e) {
				logger.warning("Can't open " + Configuration.bootstrapNodeFile + " for writing default bootstrap destination");
			}
		}
		
	}
	
	public static I2PNetwork getInstance() {
	    if(instance == null) {
	        try {
				instance = new I2PNetwork();
			} catch (I2PSessionException e) {
				logger.severe("Can't open the I2P session for listening.");
			} catch (IOException e) {
				logger.warning("Can't open the file for kad id");
			}
	    }
	    
	    return instance;
	}

	public void initialize(I2PSession session) throws I2PSessionException, IOException {
		logger.info("Initialization of the network");
		this.session = session;
		kad = new Kademlia(Identifier.randomIdentifier(), session);
	}
	
	public void connectTo(String dest) throws IOException, I2PException {
		Destination destination = new Destination();
		destination.fromBase64(dest);
		connectTo(destination);
	}
	
	public void connectTo(Destination dest) throws IOException, I2PException {
		logger.info("Connecting to the destination " + dest.toBase64());
		kad.connect(dest);
		connected = true;
	}

	public boolean isConnected() {
		return connected;
	}
	
	public Destination getDestination() {
		return kad.getLocalDestination();
	}
	
	public synchronized String resolvFromName(String name) throws IOException, I2PException {
		Serializable result = kad.get(new Identifier(name));
		
		@SuppressWarnings("unchecked")
		ArrayList<TimestampedValue> maybeDests = (ArrayList<TimestampedValue>) result;
		
		if (maybeDests == null) 
			throw new IOException("No destinations found into DHT for " + name);
		
		for (int i=0; i<maybeDests.size(); i++) {
			if (maybeDests.get(i).verifyCorrectness(new Identifier(name))) {
				Destination realDest = new Destination();
				realDest.fromBase64((String) maybeDests.get(i).getObject());
				return realDest.toBase64();
			}
		}
		
		throw new IOException("No destinations found into DHT for " + name);
	}
	
	
	
	
	
	
	public void sendMessage(String message, Destination dest)
			throws IOException, I2PException {
		ByteArrayOutputStream bout = new ByteArrayOutputStream();
		DataOutputStream dout = new DataOutputStream(bout);
		dout.write(message.getBytes());
		dout.close();

		byte[] data = bout.toByteArray();
		if (data.length > 1024) {
			throw new IOException("Message too big, size="+data.length+
					" bytes, max="+1024+" bytes");
		}

		I2PDatagramMaker maker = new I2PDatagramMaker(session);
		session.sendMessage(dest, maker.makeI2PDatagram(data), I2PSession.PROTO_DATAGRAM, 400, 405);
	}
}
