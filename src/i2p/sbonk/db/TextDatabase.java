/** 
 *   (c) danimoth, 2011 
 * 
 *   This file is part of Sbonk.
 *
 *   Sbonk is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Sbonk is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Sbonk.  If not, see <http://www.gnu.org/licenses/>.
 */
package i2p.sbonk.db;

import i2p.sbonk.Configuration;
import i2p.sbonk.Post;
import i2p.sbonk.RemoteSbonker;
import i2p.sbonk.Sbonker;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.LinkedList;
import java.util.logging.Logger;

import net.i2p.data.DataFormatException;


/** Implements a Database via plain text files
 * <p>
 * The implementation follows KISS philosophy. All posts are
 * saved into a directory as plain text files, which have the
 * post hash as name, and plain-written text and root hash inside.
 * 
 * @author danimoth
 *
 */
public class TextDatabase implements Database {
	
	private static TextDatabase instance = null;
	
	private static Logger logger = Logger.getLogger("TextDatabase");
	
	/** Gets an instance of the TextDabase
	 * 
	 * @return TextDatabase istance
	 */
	public static TextDatabase getInstance() {
	    if(instance == null) {
	        try {
				instance = new TextDatabase();
			} catch (IOException e) {
				logger.severe(e.getMessage());
			}
	    }
	    
	    return instance;
	}
	
	/** Construct a new TextDabase
	 * 
	 * Open the database path provided in the configuration.
	 * 
	 * @throws IOException
	 */
	protected TextDatabase() throws IOException {
		logger.info("Initialization of the database on the path " + Configuration.databasePath);
		
		File dir = new File(Configuration.databasePath);
		
		if (!dir.exists()) {
			dir.mkdir();
		} else if (!dir.isDirectory()) {
			IOException e = new IOException(Configuration.databasePath + " isn't a directory");
			throw e;
		}
		
		if (!dir.canRead()) {
			IOException e = new IOException("You have not reading permission on " + Configuration.databasePath);
			throw e;
		}
		
		if (!dir.canWrite()) {
			IOException e = new IOException("You have not writing permission on " + Configuration.databasePath);
			throw e;
		}
	}
	
	
	@Override
	public LinkedList<Sbonker> getAllSbonkers() {
		LinkedList<Sbonker> qe = new LinkedList<Sbonker>();
		
		File f = new File(Configuration.databasePath);
		String[] list = f.list();
		
		if (list == null) {
			return qe;
		}
		
		for (int i = 0; i<list.length; i++) {
				try {
					Sbonker sbk;
					sbk = new RemoteSbonker(new FileInputStream(Configuration.databasePath+ list[i] + "/main_profile.dat"));
					qe.add(sbk);
				} catch (DataFormatException e) {
					logger.warning("DataFormatException when reading the Sbonker " + list[i] + 
							" from the local database. Error: " + e.getMessage());
				} catch (FileNotFoundException e) {
					logger.warning("FileNotFound when reading the Sbonker " + list[i] + 
							" from the local database. Error: " + e.getMessage());
				} catch (IOException e) {
					logger.warning("IOException when reading the Sbonker " + list[i] + 
							" from the local database. Error: " + e.getMessage());
				}
		}
		return qe;
	}
	
	@Override
	public OutputStream getOutputStreamForPost(Sbonker sbk, Post post) {
		File f = new File(Configuration.databasePath + sbk.getName() + "/");
		
		if (!f.exists()) {
			f.mkdir();
		}
		
		try {
			return new FileOutputStream(f+"/"+post.getHash());
		} catch (FileNotFoundException e) {
			return null;
		}
	}

	@Override
	public OutputStream getOutputStreamForSbonker(Sbonker sbk) {
		File f = new File(Configuration.databasePath + sbk.getName() + "/");
	
		if (!f.exists()) {
			f.mkdir();
		}
		
		try {
			return new FileOutputStream(f+"/main_profile.dat");
		} catch (FileNotFoundException e) {
			return null;
		}
	}

	@Override
	public int getPostCount(Sbonker sbk) {
		File f = new File(Configuration.databasePath + sbk.getName());
		String[] list = f.list();
		
		// main_profile.dat isn't a post
		return list.length - 1;
	}

	@Override
	public LinkedList<Post> getAllPosts(Sbonker sbk) {
		LinkedList<Post> qe = new LinkedList<Post>();

		File f = new File(Configuration.databasePath + sbk.getName());
		String[] list = f.list();

		if (list == null) {
			return qe;
		}

		for (int i = 0; i < list.length; i++) {
			if (list[i].equals("main_profile.dat"))
				continue;
			
			try {
				Post pst = new Post(new FileInputStream(
						Configuration.databasePath + sbk.getName() + "/" +list[i]));
				qe.add(pst);
			} catch (Exception e) {
				logger.warning("Error when loading a post ("
						+ list[i] + ") from the database: " + e.getMessage());
			}
		}
		return qe;
	}
}
