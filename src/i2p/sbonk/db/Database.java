/** 
 *   (c) danimoth, 2011 
 * 
 *   This file is part of Sbonk.
 *
 *   Sbonk is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Sbonk is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Sbonk.  If not, see <http://www.gnu.org/licenses/>.
 */
package i2p.sbonk.db;

import i2p.sbonk.Post;
import i2p.sbonk.Sbonker;

import java.io.OutputStream;
import java.util.LinkedList;

/**  The Database interface
 * <p>
 * The database is the location where all the posts are saved. 
 * Implementation technology is arbitrary; that is,
 * several implementations are possible (via files, database, DHT..):
 * all you have to do is to provide the relative API below.
 * 
 * <p> Design <p>
 * 
 * The database must rely on the Post ability to do its work, therefore write
 * and read operations must be accomplished via Post API. The thing that really
 * matters is where the things are written and read.
 * 
 * Database must also provide coherence and time-concurrency. Also, a 
 * failure-resist mechanism is required.
 * 
 * The Database implementation could also do caching.
 * @author danimoth
 *
 */
public interface Database {
	
	/** Get a list of all Sbonkers saved into the Database
	 * 
	 * @return list of all Sbonkers
	 */
	public LinkedList<Sbonker> getAllSbonkers();
	
	/** Output stream where save a Post from a Sbonker
	 * 
	 * @param sbk Sbonker which did the post
	 * @param post Post made by the Sbonker
	 * @return
	 */
	public OutputStream getOutputStreamForPost(Sbonker sbk, Post post);
	
	/** Output stream where save the Sbonker
	 * 
	 * @param sbk Sbonker to save
	 * @return output stream where writing the sbonker
	 */
	public OutputStream getOutputStreamForSbonker(Sbonker sbk);
	
	/** Get the count of post saved in the local database for the sbonker sbk
	 * 
	 * @param sbk Sbonker of which take the post count
	 * @return Number of the post saved locally
	 */
	public int getPostCount(Sbonker sbk);
	
	/** Get all the post saved in the local database for the sbonker sbk
	 * 
	 * @param sbk Sbonker of which take the posts
	 * @return A list of locally-saved Post
	 */
	public LinkedList<Post> getAllPosts(Sbonker sbk);
}
