package i2p.sbonk.kad.messaging;

import i2p.sbonk.kad.operation.OriginMessage;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import net.i2p.I2PException;
import net.i2p.client.I2PSession;
import net.i2p.client.I2PSessionException;
import net.i2p.client.I2PSessionListener;
import net.i2p.client.datagram.I2PDatagramDissector;
import net.i2p.client.datagram.I2PDatagramMaker;
import net.i2p.data.Destination;

/**
 * Listens for incoming UDP messages and provides a framework for sending messages
 * and responding to received messages.
 * Two threads are started: One that listens for incoming messages and one that
 * handles timeout events.
 **/
public class MessageServer {
    /**
     * Maximum size of a received datagram packet.
     * Could probably be less, but is set too high to be on the safe side.
     **/
    public static final int DATAGRAM_BUFFER_SIZE = 10*1024;

    private static Random random = new Random();
    private MessageFactory factory;
    private long timeout;
    private I2PSession session;
    private Timer timer;
    private Listener list;
    private Map<Integer, Receiver> receivers; // keeps track of registered receivers
    private Map<Integer, TimerTask> tasks;     // keeps track of timeout events

    /**
     * Constructs a MessageServer listening on the specified UDP port using the
     * specified MessageFactory for interpreting incoming messages.
     *
     * @param session I2PSession where send/receive messages
     * @param factory Factory for creating Message and Receiver objects
     * @param timeout The timeout period in milliseconds
     *
     * @throws I2PSessionException if the session can't be opened
     **/
    public MessageServer(I2PSession session, MessageFactory factory, long timeout) throws I2PSessionException {
        this.factory = factory;
        this.timeout = timeout;
        this.session = session;
        timer = new Timer(true);
        receivers = new HashMap<Integer, Receiver>();
        tasks = new HashMap<Integer, TimerTask>();
        list = new Listener(this);
        session.addSessionListener(list, I2PSession.PROTO_DATAGRAM, I2PSession.PORT_ANY);
		session.connect();
    }

    /**
     * Sends the specified Message and calls the specified Receiver when a reply
     * for the message is received. If <code>recv</code> is <code>null</code>
     * any reply is ignored. Returns a unique communication id which can be used
     * to identify a reply.
     * @throws I2PException 
     **/
	public synchronized int send(Message message, Destination dest, Receiver recv) throws IOException, I2PException {
        if (!list.isRunning()) throw new IllegalStateException("MessageServer not running");
        int comm = random.nextInt();
        if (recv != null) {
            Integer key = new Integer(comm);
            receivers.put(key, recv);
            TimerTask task = new TimeoutTask(comm, recv);
            timer.schedule(task, timeout);
            tasks.put(key, task);
        }
        sendMessage(comm, message, dest);
        return comm;
    }

    /**
     * Sends a reply to the message with the specified communication id.
     * @throws I2PException 
     **/
    public synchronized void reply(int comm, Message message, Destination dest) 
    										throws IOException, I2PException {
        if (!list.isRunning()) throw new IllegalStateException("MessageServer not running");
        sendMessage(comm, message, dest);
    }

    private void sendMessage(int comm, Message message, Destination dest)
                                       							throws IOException, I2PException {
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        DataOutputStream dout = new DataOutputStream(bout);
        dout.writeInt(comm);
        dout.writeByte(message.code());
        message.toStream(dout);
        dout.close();

        byte[] data = bout.toByteArray();
        if (data.length > DATAGRAM_BUFFER_SIZE) {
            throw new IOException("Message too big, size="+data.length+
                                  " bytes, max="+DATAGRAM_BUFFER_SIZE+" bytes");
        }

        I2PDatagramMaker maker = new I2PDatagramMaker(session);
        session.sendMessage(dest, maker.makeI2PDatagram(data), I2PSession.PROTO_DATAGRAM, 400, 405);
    }

    private synchronized void unregister(int comm) {
        Integer key = new Integer(comm);
        receivers.remove(key);
        tasks.remove(key);
    }

    public void decodeMessage(byte[] buffer) throws Exception {
    	// Decode data into Message object
        I2PDatagramDissector diss = new I2PDatagramDissector();
        diss.loadI2PDatagram(buffer);
        ByteArrayInputStream bin = null;
		bin = new ByteArrayInputStream(diss.getPayload());
		DataInputStream din = new DataInputStream(bin);
        Destination whoSent = diss.getSender();
        
        int comm;
        comm = din.readInt();
        byte messCode = din.readByte();
        
        Message message = factory.createMessage(messCode, din);
        din.close();

        // sanity check
        OriginMessage msg = (OriginMessage) message;
        if (msg != null) {
        	if (! msg.getOrigin().getDestination().equals(whoSent)) {
        		System.out.println("Someone want to spoof him. Aborting reply to message id 0x" + messCode);
        		return;
        	}
        }

        // Create Receiver if one is supported
        Receiver recv = null;
        recv = factory.createReceiver(messCode, this);

        // If no receiver, get registered Receiver, if any
        if (recv == null) {
        	synchronized (this) {
        		Integer key = new Integer(comm);
        		recv = (Receiver) receivers.remove(key);
        		// Cancel timer if there was a registered Receiver
        		if (recv != null) {
        			TimerTask task = (TimerTask) tasks.remove(key);
        			task.cancel();
        		}
        	}
        }

        // Invoke Receiver if one was found
        if (recv != null) {
        	recv.receive(message, comm);
        }
    }
    /**
     * Signals to the MessageServer thread that it should stop running.
     **/
    public synchronized void close() {
        if (!list.isRunning()) throw new IllegalStateException("MessageServer not running");
        try {
			session.destroySession();
		} catch (I2PSessionException e) {
			// LOL.. why exception here?
		}
        timer.cancel();
        receivers.clear();
        tasks.clear();
    }

    /**
     * Task that gets called by a separate thread if a timeout for a receiver occurs.
     * When a reply arrives this task must be cancelled using the <code>cancel()</code>
     * method inherited from <code>TimerTask</code>. In this case the caller is
     * responsible for removing the task from the <code>tasks</code> map.
     **/
    class TimeoutTask extends TimerTask {
        private int comm;
        private Receiver recv;

        public TimeoutTask(int comm, Receiver recv) {
            this.comm = comm;
            this.recv = recv;
        }

        public void run() {
            try {
                unregister(comm);
                recv.timeout(comm);
            } catch (Exception e) {
				System.out.println("Got a timeout: " + e.getMessage());
			}
        }
    }
}

class Listener implements I2PSessionListener {

	private MessageServer server;
	private boolean isRunning = true;
	
	public Listener (MessageServer server) {
		this.server = server;
	}
	
	public boolean isRunning() {
		return isRunning;
	}
	
	@Override
	public void disconnected(I2PSession arg0) {
		isRunning = false;
	}

	@Override
	public void errorOccurred(I2PSession session, String message, Throwable error) {
		isRunning = false;
	}

	@Override
	public void messageAvailable(I2PSession session, int msgId, long size) {
    	@SuppressWarnings("static-access")
		byte[] buffer = new byte[server.DATAGRAM_BUFFER_SIZE];
    	
    	try {
			buffer = session.receiveMessage(msgId);
	        server.decodeMessage(buffer);
		} catch (Exception e) {
			System.out.println("Exception when receiving a message: " + e.getLocalizedMessage());
		}
	}

	@Override
	public void reportAbuse(I2PSession arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}
	
}