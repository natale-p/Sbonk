package i2p.sbonk.kad;

import java.io.*;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.commons.collections.MultiMap;
import org.apache.commons.collections.map.MultiValueMap;

import net.i2p.I2PException;
import net.i2p.client.I2PSession;
import net.i2p.client.I2PSessionException;
import net.i2p.data.Destination;
import i2p.sbonk.kad.messaging.*;
import i2p.sbonk.kad.operation.*;

/**
 * Maps keys to values in a distributed setting using the Kademlia protocol.
 * Three threads are started: One that listens for incoming routing messages,
 * one that handles timeouts for routing messages, and one that handles
 * hourly refresh/restore.
 **/
public class Kademlia implements DistributedMap {
    private Configuration conf;
    private MultiMap localMap;
    private String name;
    private Node local;
    private Space space;
    private MessageServer server;
    private Timer timer;
    private boolean isClosed = false;

    /**
     * Creates a Kademlia DistributedMap with the specified identifier and no
     * persistence.
     *
     * @param id         The id to use or null to create a random id
     * @param manager    I2P Socket manager
     * @throws I2PSessionException 
     * @throws I2PException 
     **/
    public Kademlia(Identifier id, I2PSession session) throws IOException, I2PSessionException {
        this(null, id, session, null);
    }

    /**
     * Creates a Kademlia DistributedMap with the specified identifier and no
     * persistence.
     *
     * @param id         The id to use or null to create a random id
     * @param manager    I2P Socket manager
     * @param config     Configuration parameters
     * @throws I2PSessionException 
     * @throws I2PException 
     **/
    public Kademlia(Identifier id, I2PSession session, Configuration config)
                              throws IOException, I2PSessionException {
        this(null, id, session, config);
    }

    /**
     * Creates a Kademlia DistributedMap using the specified name as filename base.
     * If the id cannot be read from disk a random id is created.
     * The instance is bootstraped to an existing network by specifying the
     * address of a bootstrap node in the network.
     *
     * @param name       Filename base or null if persistence should not be used
     * @param session    I2P Socket manager
     * @param config     Configuration parameters. If null default parameters are used.
     * @throws I2PSessionException 
     **/
    public Kademlia(String name, I2PSession session,
                    Configuration config) throws IOException, I2PSessionException {
        this(name, null, session, config);
    }

    /**
     * Creates a Kademlia DistributedMap using the specified name as filename base.
     * If the id cannot be read from disk the specified defaultId is used.
     * The instance is bootstraped to an existing network by specifying the
     * address of a bootstrap node in the network.
     *
     * @param name       Filename base or null if persistence should not be used
     * @param defaultId  Default id if it could not be read from disk or null for
     *                   random default id
     * @param session    I2P Socket manager
     * @param config     Configuration parameters. If null default parameters are used.
     *
     * @throws I2PException     If the bootstrap node did not respond
     * @throws IOException      If an error occurred while reading id or local map
     *                          from disk <i>or</i> a network error occurred while
     *                          attempting to connect to the network
     * @throws I2PSessionException 
     **/
	public Kademlia(String name, Identifier defaultId, I2PSession session, Configuration config)
                   throws IOException, I2PSessionException {

        conf = (config == null) ? new Configuration() : config;

        if (defaultId == null) defaultId = Identifier.randomIdentifier();
        Identifier id = defaultId;
        localMap = new MultiValueMap();

        // Read id and localMap from disk or start with specified id and empty map
        this.name = name;
        if (name != null) id = initFromDisk(defaultId);
        
        local = new Node(session.getMyDestination(), id);
        NeighbourhoodListenerImpl listener = new NeighbourhoodListenerImpl(local);
        space = new Space(local, conf, listener);

        MessageFactory factory = new MessageFactoryImpl(localMap, local, space);
        server = new MessageServer(session, factory, conf.RESPONSE_TIMEOUT);
        listener.setMessageServer(server);

        // Schedule recurring RestoreOperation
        timer = new Timer(true);
        timer.schedule(
            new TimerTask() {
                public void run() {
                    Operation op = new RestoreOperation(conf, server, space,
                                                           local, localMap);
                    try {
						op.execute();
					} catch (IOException e) {
						e.printStackTrace();
					} catch (I2PException e) {
						System.out.println("Networking error in scheduled restoreOperation - " +
								e.getLocalizedMessage());
					}
                }
            },
            conf.RESTORE_INTERVAL, conf.RESTORE_INTERVAL
        );
    }

	private Identifier initFromDisk(Identifier defaultId) {
        // Read id from disk or create new random id
        Identifier id = null;
        try {
            String idname = name+".id";
            DataInputStream in = new DataInputStream(new FileInputStream(idname));
            id = new Identifier(in);
            in.close();
        } catch (IOException e) {
            id = defaultId;
        }

        // Read local map from disk or start empty
        try {
            String mapname = name+".map";
            ObjectInputStream in = new ObjectInputStream(new BufferedInputStream(
                                                   new FileInputStream(mapname)));
            localMap = (MultiMap) in.readObject();
            in.close();
        } catch (IOException e) {
            localMap = new MultiValueMap();
        } catch (ClassNotFoundException e) {
            localMap = new MultiValueMap();
        }
        return id;
    }

    private void stateToDisk() throws IOException {
        // Write id to disk
        String idname = name+".id";
        DataOutputStream dout = new DataOutputStream(new FileOutputStream(idname));
        local.getId().toStream(dout);
        dout.close();

        // Write local map to disk
        String mapname = name+".map";
        ObjectOutputStream out = new ObjectOutputStream(new BufferedOutputStream(
                                                  new FileOutputStream(mapname)));
        out.writeObject(localMap);
        out.close();
    }

    /**
     * Attempts to connect to an existing peer-to-peer network.
     *
     * @param bootstrap  The destination of a known node in the peer-to-peer network
     *
     * @throws I2PException          If the bootstrap node could not be contacted
     * @throws IOException           If a network error occurred
     * @throws IllegalStateException If this object is closed
     **/
    public void connect(Destination bootstrap) throws IOException, I2PException {
        if (isClosed) throw new IllegalStateException("Kademlia instance is closed");
        Operation op = new ConnectOperation(conf, server, space, local, bootstrap);
        op.execute();
    }

    /**
     * Closes the map. Any subsequent calls to methods are invalid.
     *
     * @throws IOException           If an error occurred while writing data to disk
     * @throws IllegalStateException If this object is closed
     **/
    public void close() throws IOException {
        if (isClosed) throw new IllegalStateException("Kademlia instance is closed");
        isClosed = true;
        timer.cancel();
        server.close();
        if (name != null) stateToDisk();
    }

    /**
     * Returns <code>true</code> if the map contains the specified key and
     * <code>false</code> otherwise.
     *
     * @param  key The key to lookup
     * @return <code>true</code> if a mappings with the key was found and
     *         <code>false</code> otherwise.
     *
     * @throws I2PException          If the lookup operation timed out
     * @throws IOException           If a network error occurred
     * @throws IllegalStateException If this object is closed
     **/
    public boolean contains(Identifier key) throws IOException, I2PException {
        if (isClosed) throw new IllegalStateException("Kademlia instance is closed");
        return (get(key) != null);
    }

    /**
     * Returns the value associated with the specified key.
     * If the value is stored locally no Kademlia lookup is performed.
     * See also {@link DataLookupOperation}.
     *
     * @param  key The key to lookup
     * @return The value mapped to the key or <code>null</code> if no value
     *         is mapped to the key.
     *
     * @throws I2PException          If the lookup operation timed out
     * @throws IOException           If a network error occurred
     * @throws IllegalStateException If this object is closed
     **/
    public Serializable get(Identifier key) throws IOException, I2PException {
        if (isClosed) throw new IllegalStateException("Kademlia instance is closed");
        synchronized (localMap) {
            if (localMap.containsKey(key)) {
                return (Serializable) localMap.get(key);
            }
        }
        Operation op = new DataLookupOperation(conf, server, space, local, key);
        return (Serializable) op.execute();
    }

    /**
     * Associates the specified value with the specified key.
     * It is guaranteed that the mapping is stored on
     * <i>K</i> nodes or all nodes if less than
     * this number of nodes exist in the network. See also {@link StoreOperation}.
     *
     * @param  key   The key
     * @param  value The value associated with the key
     * @param  type  The type of the value: 0x1 for DNS
     *
     * @throws I2PException          If the operation timed out
     * @throws IOException           If a network error occurred
     * @throws IllegalStateException If this object is closed
     **/
    public void put(Identifier key, Serializable value, byte type) throws IOException, I2PException {
        if (isClosed) throw new IllegalStateException("Kademlia instance is closed");
        long now = System.currentTimeMillis();
        TimestampedValue timeval;
        
        switch(type) {
        case TimestampedValue.T_DNS:
        	timeval = new DNSValue(value, now);
        	break;
        default:
        	return;
        }
        
        Operation op = new StoreOperation(conf, server, space, localMap, local, key, timeval);
        op.execute();
    }

    /**
     * Removes the mapping with the specified key.
     *
     * @param  key  The key of the mapping to remove
     *
     * @throws I2PException          If the operation timed out
     * @throws IOException           If a network error occurred
     * @throws IllegalStateException If this object is closed
     **/
    public void remove(Identifier key) throws IOException, I2PException {
        if (isClosed) throw new IllegalStateException("Kademlia instance is closed");
        Operation op = new RemoveOperation(conf, server, space, localMap, local, key);
        op.execute();
    }
    
    public Destination getLocalDestination() {
    	return local.getDestination();
    }

    ///////////////////////////////////////////////////////////////////////////
    /////////////////////// Methods for Testing Purposes //////////////////////
    ///////////////////////////////////////////////////////////////////////////

    /**
     * For debugging purposes, returns the contents of the internal space.
     **/
    public String toString() {
        return local.getDestination().toString()+
               "map size="+localMap.size()+", "+
               "space size="+space.nodeCount();
    }

    public Space internalGetSpace() {
        return space;
    }

    Node internalGetLocal() {
        return local;
    }

    public MultiMap internalGetMap() {
        return localMap;
    }
}
