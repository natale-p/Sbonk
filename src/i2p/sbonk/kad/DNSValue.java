package i2p.sbonk.kad;

import java.io.DataInput;
import java.io.IOException;
import java.io.Serializable;

import net.i2p.data.DataFormatException;
import net.i2p.data.Destination;

public class DNSValue extends TimestampedValue{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2269502274037558738L;

	public DNSValue(DataInput in) throws IOException {
		super(in);
	}
	
	public DNSValue(Serializable obj, long timestamp) throws IOException {
		super(obj, timestamp);
	}

	@Override
	public byte code() {
		return T_DNS;
	}

	@Override
	public boolean verifyCorrectness(Identifier key) {
		String[] myKey = (new String(key.toByteArray()).split(":"));

		if (myKey.length != 2)
			return false;
		
		String value;
		try {
			Object o = getObject();
			if (o instanceof String) {
				value = (String) o;
			} else 
				return false;
		} catch (IOException e) {
			return false;
		}
		
		try {
			Destination dest = new Destination();
			dest.fromBase64(value);
			return (dest.getHash().toBase64().substring(0,4).equals(myKey[1]));
		} catch (StringIndexOutOfBoundsException e) {
			return false;
		} catch (DataFormatException e) {
			return false;
		}
	}
}
