package i2p.sbonk.kad.operation;

import java.io.IOException;
import net.i2p.I2PException;
import net.i2p.data.Destination;
import i2p.sbonk.kad.messaging.*;
import i2p.sbonk.kad.Configuration;
import i2p.sbonk.kad.Node;
import i2p.sbonk.kad.Space;

/**
 * Connects to an existing Kademlia network using a bootstrap node.
 **/
public class ConnectOperation extends Operation implements Receiver {
    private static final int MAX_ATTEMPTS = 5;

    private Configuration conf;
    private MessageServer server;
    private Space space;
    private Node local;
    private Destination destination;
    private boolean error;
    private int attempts;

    public ConnectOperation(Configuration conf, MessageServer server, Space space,
                            Node local, Destination dest) {
        this.conf = conf;
        this.server = server;
        this.space = space;
        this.local = local;
        this.destination = dest;
    }

    /**
     * @return <code>null</code>
     * @throws IOException      if a network error occurred
     * @throws I2PException     if the bootstrap node did not respond
     **/
    public synchronized Object execute() throws IOException, I2PException {
        try {
            // Contact bootstrap node
            error = true;
            attempts = 0;
            Message mess = new ConnectMessage(local);
            server.send(mess, destination, this);
            wait(conf.OPERATION_TIMEOUT);
            if (error) {
                throw new I2PException("Bootstrap node did not respond: "+destination.getHash());
            }

            // Perform lookup operation for own id
            Operation lookup = new NodeLookupOperation(conf, server, space,
                                                       local, local.getId());
            lookup.execute();

            // Refresh buckets
            Operation refresh = new RefreshOperation(conf, server, space, local);
            refresh.execute();

            return null;
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Receives an AcknowledgeMessage from the boot strap node.
     **/
    public synchronized void receive(Message incoming, int comm) throws IOException {
        AcknowledgeMessage mess = (AcknowledgeMessage) incoming;
        // Insert bootstrap node in space (id is now known)
        space.insertNode(mess.getOrigin());
        error = false;
        notify();
    }

    /**
     * Resends a ConnectMessage to the boot strap node a maximum of MAX_ATTEMPTS
     * times.
     * @throws I2PException 
     **/
    public synchronized void timeout(int comm) throws IOException, I2PException {
        if (++attempts < MAX_ATTEMPTS) {
            Message mess = new ConnectMessage(local);
            server.send(mess, destination, this);
        } else {
            notify();
        }
    }
}
