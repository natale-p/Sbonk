package i2p.sbonk.kad.operation;

import java.io.IOException;
import java.io.Serializable;

import net.i2p.I2PException;
import i2p.sbonk.kad.Configuration;
import i2p.sbonk.kad.Identifier;
import i2p.sbonk.kad.Node;
import i2p.sbonk.kad.Space;
import i2p.sbonk.kad.messaging.*;

/**
 * Looks up the specified identifier and returns the value associated with it.
 * It is not checked if the data is available in the local IdentifierMap, so
 * this should be checked before this operation is executed.
 * The subclassing is merely for code reuse.
 **/
public class DataLookupOperation extends NodeLookupOperation {
    /** The found value or <code>null</code> if it has not been found. */
    protected Serializable value = null;

    public DataLookupOperation(Configuration conf, MessageServer server, Space space,
                                                           Node local, Identifier id) {
        super(conf, server, space, local, id);
        // Change lookup message sent to peers
        lookupMessage = new DataLookupMessage(local, id);
    }

    /**
     * @return The value associated with the key or <code>null</code> if no
     *         mapping with the specified key could be found.
     *
     * @throws RoutingException If the lookup operation timed out
     * @throws IOException      If a network error occurred
     * @throws I2PException 
     **/
    public synchronized Object execute() throws IOException, I2PException {
        super.execute();
        return value;
    }

    /**
     * Once a DataMessage is received the algorithm terminates.
     * If a NodeReplyMessage is received the algorithm continues as by
     * the superclass.
     * @throws I2PException 
     **/
    public synchronized void receive(Message incoming, int comm) throws IOException, I2PException {
        if (incoming instanceof DataMessage) {
            DataMessage mess = (DataMessage) incoming;
            space.insertNode(mess.getOrigin());
            if (id.equals(mess.getKey())) {
                value = mess.getValue();
                error = false;
                notify();
            } else {
                throw new IOException("Key in DataMessage received does not "+
                                      "match looked up key");
            }
        } else {
            super.receive(incoming, comm);
        }
    }
}
