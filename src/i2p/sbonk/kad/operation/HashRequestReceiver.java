package i2p.sbonk.kad.operation;

import i2p.sbonk.kad.HashCalculator;
import i2p.sbonk.kad.Node;
import i2p.sbonk.kad.Space;
import i2p.sbonk.kad.messaging.Message;
import i2p.sbonk.kad.messaging.MessageServer;
import i2p.sbonk.kad.messaging.UnknownMessageException;

import java.io.IOException;
import java.util.List;

import net.i2p.I2PException;

import org.apache.commons.collections.MultiMap;

/**
 * Receives a HashRequestMessage and replies with a HashMessage containing
 * group hashes of mappings that should both be available at the local node
 * and the origin node.
 **/
public class HashRequestReceiver extends OriginReceiver {
    private MultiMap localMap;
    private HashCalculator hasher;

    public HashRequestReceiver(MessageServer server, Node local, Space space, 
    							MultiMap localMap) {
        super(server, local, space);
        this.localMap = localMap;
        hasher = new HashCalculator(local, space, localMap);
    }

    public void receive(Message incoming, int comm) throws IOException,
                                               UnknownMessageException, I2PException {
        super.receive(incoming, comm);
        HashRequestMessage mess = (HashRequestMessage) incoming;
        Node origin = mess.getOrigin();

        long now = System.currentTimeMillis();
        List<byte[]> hashes = hasher.logarithmicHashes(origin, now);

        if (hashes.size() > 0) {
            Message rep = new HashMessage(local, now, hashes);
            server.reply(comm, rep, origin.getDestination());
        }
    }
}
