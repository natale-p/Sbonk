package i2p.sbonk.kad.operation;

import i2p.sbonk.kad.HashCalculator;
import i2p.sbonk.kad.Node;
import i2p.sbonk.kad.Space;
import i2p.sbonk.kad.TimestampedEntry;
import i2p.sbonk.kad.messaging.Message;
import i2p.sbonk.kad.messaging.MessageServer;

import java.io.IOException;
import java.util.List;

import net.i2p.I2PException;

import org.apache.commons.collections.MultiMap;

/**
 * Receives an incoming StoreRequestMessage and issues StoreMessages for
 * mappings in the requested interval that should be available at the
 * origin node.
 **/
public class StoreRequestReceiver extends OriginReceiver {
    private MultiMap localMap;
    private HashCalculator hasher;

    public StoreRequestReceiver(MessageServer server, Node local, Space space,
    							MultiMap localMap) {
        super(server, local, space);
        this.localMap = localMap;
        hasher = new HashCalculator(local, space, localMap);
    }

    public void receive(Message incoming, int comm) throws IOException, I2PException {
        super.receive(incoming, comm);
        StoreRequestMessage mess = (StoreRequestMessage) incoming;
        Node origin = mess.getOrigin();

        List<TimestampedEntry> mappings = hasher.mappingsBetween(origin, mess.getBegin(), mess.getEnd());
        for (int i=0,max=mappings.size(); i<max; i++) {
            TimestampedEntry entry = mappings.get(i);
            Message rep = new StoreMessage(local,entry.getKey(),entry.getValue(),true);
            server.send(rep, origin.getDestination(), null);
        }
    }
}
