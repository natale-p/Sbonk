package i2p.sbonk.kad.operation;

import i2p.sbonk.kad.Node;
import i2p.sbonk.kad.Space;
import i2p.sbonk.kad.messaging.Message;
import i2p.sbonk.kad.messaging.MessageServer;

import java.io.IOException;

import net.i2p.I2PException;

import org.apache.commons.collections.MultiMap;

/**
 * Handles incoming remove messages by removing the mapping with the specified
 * key from the local map.
 **/
public class RemoveReceiver extends OriginReceiver {
    private MultiMap localMap;

    public RemoveReceiver(MessageServer server, Node local, Space space, MultiMap localMap) {
        super(server, local, space);
        this.localMap = localMap;
    }

    public void receive(Message incoming, int comm) throws IOException, I2PException {
        super.receive(incoming, comm);
        RemoveMessage mess = (RemoveMessage) incoming;
        localMap.remove(mess.getKey());
    }
}
