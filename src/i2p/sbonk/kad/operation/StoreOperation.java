package i2p.sbonk.kad.operation;

import i2p.sbonk.kad.Configuration;
import i2p.sbonk.kad.Identifier;
import i2p.sbonk.kad.Node;
import i2p.sbonk.kad.Space;
import i2p.sbonk.kad.TimestampedValue;
import i2p.sbonk.kad.messaging.Message;
import i2p.sbonk.kad.messaging.MessageServer;

import java.io.IOException;
import java.util.List;

import org.apache.commons.collections.MultiMap;

import net.i2p.I2PException;

/**
 * Stores the mapping at the <i>K</i> closest nodes to the key.
 * If less than Space.K nodes exist in the network the mapping
 * will be stored at all nodes.
 **/
public class StoreOperation extends Operation {
    private Configuration conf;
    private MessageServer server;
    private Space space;
    private MultiMap localMap;
    private Node local;
    private Identifier key;
    private TimestampedValue value;

    public StoreOperation(Configuration conf, MessageServer server, Space space,
    					MultiMap localMap, Node local, Identifier key, TimestampedValue value) {
        if (key == null) throw new NullPointerException("Key is null");
        if (value == null) throw new NullPointerException("Value is null");
        this.conf = conf;
        this.server = server;
        this.space = space;
        this.localMap = localMap;
        this.local = local;
        this.key = key;
        this.value = value;
    }

    /**
     * @return <code>null</code>
     * @throws I2PException     If a network error occurred or the operation timed out
     * @throws IOException      Value isn't correct for the key
     **/
    public synchronized Object execute() throws IOException, I2PException {
        // Perform lookup for K closest nodes
        Operation op = new NodeLookupOperation(conf, server, space, local, key);
        @SuppressWarnings("unchecked")
		List<Node> nodes = (List<Node>) op.execute();

        if (! value.verifyCorrectness(key)) {
        	throw new IOException("Bad message of type 0x" + value.code() + ". Not storing");
        }

        // Send store message to K closest nodes
        Message message = new StoreMessage(local, key, value, false);
        for (int i=0, max=nodes.size(); i < max; i++) {
            Node node = nodes.get(i);
            if (node.equals(local)) {
            	localMap.put(key, value);
            } else {
            	server.send(message, node.getDestination(), null);
            }
        }
        return null;
    }
}
