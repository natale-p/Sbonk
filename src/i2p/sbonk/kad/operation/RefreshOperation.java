package i2p.sbonk.kad.operation;

import java.io.IOException;
import java.util.List;

import net.i2p.I2PException;
import i2p.sbonk.kad.messaging.*;
import i2p.sbonk.kad.Configuration;
import i2p.sbonk.kad.Identifier;
import i2p.sbonk.kad.Node;
import i2p.sbonk.kad.Space;

/**
 * Refreshes all buckets.
 **/
public class RefreshOperation extends Operation {
    private Configuration conf;
    private MessageServer server;
    private Space space;
    private Node local;

    public RefreshOperation(Configuration conf, MessageServer server, Space space, Node local) {
        this.conf = conf;
        this.server = server;
        this.space = space;
        this.local = local;
    }

    /**
     * @return <code>null</code>
     * @throws I2PException     If a suboperation timed out
     * @throws IOException      If a network error occurred
     **/
    public synchronized Object execute() throws IOException, I2PException {
        List<Identifier> refreshIds = space.getRefreshList();
        for (int i = 0, max = refreshIds.size(); i < max; i++) {
            Identifier id = (Identifier) refreshIds.get(i);
            Operation op = new NodeLookupOperation(conf, server, space, local, id);
            op.execute();
        }
        return null;
    }
}

