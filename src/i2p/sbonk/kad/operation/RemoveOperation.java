package i2p.sbonk.kad.operation;

import java.io.IOException;
import java.util.List;
import org.apache.commons.collections.MultiMap;

import net.i2p.I2PException;
import i2p.sbonk.kad.messaging.*;
import i2p.sbonk.kad.Configuration;
import i2p.sbonk.kad.Node;
import i2p.sbonk.kad.Identifier;
import i2p.sbonk.kad.Space;

/**
 * Removes the mapping at the <i>K</i> closest nodes to the key of the mapping.
 * If less than Space.K nodes exist in the network the mapping will be removed
 * at all nodes.
 **/
public class RemoveOperation extends Operation {
    private Configuration conf;
    private MessageServer server;
    private Space space;
    private MultiMap localMap;
    private Node local;
    private Identifier key;

    public RemoveOperation(Configuration conf, MessageServer server, Space space,
    						MultiMap localMap, Node local, Identifier key) {
        if (key == null) throw new NullPointerException("Key is null");
        this.conf = conf;
        this.server = server;
        this.space = space;
        this.localMap = localMap;
        this.local = local;
        this.key = key;
    }

    /**
     * @return <code>null</code>
     * @throws I2PException     If the operation timed out
     * @throws IOException      If a network error occurred
     **/
    public synchronized Object execute() throws IOException, I2PException {
        // Perform lookup for K closest nodes
        Operation op = new NodeLookupOperation(conf, server, space, local, key);
        @SuppressWarnings("unchecked")
		List<Node> nodes = (List<Node>)op.execute();

        // Send remove message to K closest nodes
        Message message = new RemoveMessage(local, key);
        for (int i=0, max=nodes.size(); i < max; i++) {
            Node node = (Node) nodes.get(i);
            if (node.equals(local)) {
                localMap.remove(key);
            } else {
                server.send(message, node.getDestination(), null);
            }
        }
        return null;
    }
}
