package i2p.sbonk.kad.operation;

import java.io.DataInput;
import java.io.IOException;
import i2p.sbonk.kad.Node;

public class HashRequestMessage extends OriginMessage {
    protected HashRequestMessage() {}

    public HashRequestMessage(Node origin) {
        super(origin);
    }

    public HashRequestMessage(DataInput in) throws IOException {
        super(in);
    }

    public byte code() {
        return MessageFactoryImpl.CODE_HASH_REQUEST;
    }

    public String toString() {
        return "HashRequestMessage[origin="+origin+"]";
    }
}
