package i2p.sbonk.kad.operation;

import java.io.IOException;

import net.i2p.I2PException;
import i2p.sbonk.kad.Node;
import i2p.sbonk.kad.Space;
import i2p.sbonk.kad.messaging.*;

/**
 * Receives a ConnectMessage and sends an AcknowledgeMessage as reply.
 **/
public class ConnectReceiver extends OriginReceiver {
    public ConnectReceiver(MessageServer server, Node local, Space space) {
        super(server, local, space);
    }

    /**
     * Responds to a ConnectMessage by sending a ConnectReplyMessage.
     * @throws I2PException 
     **/
    public void receive(Message incoming, int comm) throws IOException, I2PException {
        super.receive(incoming, comm);
        ConnectMessage mess = (ConnectMessage) incoming;
        Node origin = mess.getOrigin();
        Message reply = new AcknowledgeMessage(local);
        server.reply(comm, reply, origin.getDestination());
    }
}
