package i2p.sbonk.kad.operation;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections.MultiMap;

import net.i2p.I2PException;
import i2p.sbonk.kad.HashCalculator;
import i2p.sbonk.kad.Node;
import i2p.sbonk.kad.Space;
import i2p.sbonk.kad.messaging.*;

/**
 * Receives a HashMessage and compares it to hashes generated locally.
 * Any hash for an interval not matching will result in a StoreRequestMessage
 * for that interval.
 **/
public class HashReceiver extends OriginReceiver {
	private MultiMap localMap;
    private HashCalculator hasher;

    public HashReceiver(MessageServer server, Node local, Space space, MultiMap localMap) {
        super(server, local, space);
        this.localMap = localMap;
        hasher = new HashCalculator(local, space, localMap);
    }

    public void receive(Message incoming, int comm) throws IOException,
                                               UnknownMessageException, I2PException {
        super.receive(incoming, comm);
        HashMessage mess = (HashMessage) incoming;
        Node origin = mess.getOrigin();
        List<byte[]> inhashes = mess.getHashes();
        List<byte[]> hashes = hasher.logarithmicHashes(origin, mess.getBaseTime());

        long interval = HashCalculator.UNIT_INTERVAL;
        long previous = Long.MAX_VALUE;
        long current = mess.getBaseTime();
        for (int i=0,max1=hashes.size(),max2=inhashes.size(); i<max1 && i<max2; i++) {
            byte[] hash = (byte[]) hashes.get(i);
            byte[] inhash = (byte[]) inhashes.get(i);
            if (!Arrays.equals(inhash, hash)) {
                Message req = new StoreRequestMessage(local, current, previous);
                server.send(req, origin.getDestination(), null);
            }
            previous = current;
            current -= interval;
            interval *= 2;
        }

        if (inhashes.size() > hashes.size()) {
            Message req = new StoreRequestMessage(local, Long.MIN_VALUE, previous);
            server.send(req, origin.getDestination(), null);
        }

    }
}
