package i2p.sbonk.kad.operation;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.commons.collections.MultiMap;

import net.i2p.I2PException;
import i2p.sbonk.kad.Identifier;
import i2p.sbonk.kad.Node;
import i2p.sbonk.kad.Space;
import i2p.sbonk.kad.TimestampedValue;
import i2p.sbonk.kad.messaging.*;

/**
 * Receives a StoreMessage and stores the mapping if a mapping with the same
 * key and a newer timestamp does not exist. If requested, sends a
 * StoreMessage is sent if this node has a mapping with the same key that is newer.
 **/
public class StoreReceiver extends OriginReceiver {
    private MultiMap localMap;

    public StoreReceiver(MessageServer server, Node local, Space space, 
    		             MultiMap localMap) {
        super(server, local, space);
        this.localMap = localMap;
    }

    public void receive(Message incoming, int comm) throws IOException, I2PException {
        super.receive(incoming, comm);
        
        // Store mapping
        StoreMessage mess = (StoreMessage) incoming;
        Node origin = mess.getOrigin();
        Identifier key = mess.getKey();
        TimestampedValue incmValue = mess.getValue();
        
        if (!incmValue.verifyCorrectness(key)) {
        	System.out.println("Bad message of type 0x" + incmValue.code() + " received. Not storing.TODO: blacklist origin");
        	throw new IOException("Bad message of type 0x" + incmValue.code() + " received. Not storing");
        }
        
        ArrayList<TimestampedValue> exstValue = (ArrayList<TimestampedValue>) localMap.get(key);
		
        if (exstValue == null || ! exstValue.contains(incmValue)) {
            localMap.put(key, incmValue);
        }
        
        /*if (exstValue == null || (incmValue.timestamp() > exstValue.get(0).timestamp())) {
            // Incoming mapping was newer
            localMap.put(key, incmValue);
        } else if ((exstValue != null) && mess.updateRequested() &&
                   (exstValue.get(0).timestamp() > incmValue.timestamp())) {
            // Existing mapping newer and update message requested
        	Message updmess = new StoreMessage(local, key, exstValue.get(0), false);
            server.send(updmess, origin.getDestination(), null);
        }*/
    }
}

