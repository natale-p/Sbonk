package i2p.sbonk.kad.operation;

import java.io.IOException;

import net.i2p.I2PException;

/**
 * User initiated operations should be subclasses of this class.
 **/
public abstract class Operation {
    /**
     * Starts the operation and returns when the operation is finished.
     * @throws I2PException 
     **/
    public abstract Object execute() throws IOException, I2PException;
}
