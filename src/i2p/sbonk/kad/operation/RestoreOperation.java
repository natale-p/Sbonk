package i2p.sbonk.kad.operation;

import i2p.sbonk.kad.Configuration;
import i2p.sbonk.kad.HashCalculator;
import i2p.sbonk.kad.Identifier;
import i2p.sbonk.kad.Node;
import i2p.sbonk.kad.Space;
import i2p.sbonk.kad.messaging.Message;
import i2p.sbonk.kad.messaging.MessageServer;

import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import net.i2p.I2PException;

import org.apache.commons.collections.MultiMap;

/**
 * Refreshes all buckets and sends HashMessages to all nodes that are
 * among the K closest to mappings stored at this node. Also deletes any
 * mappings that this node is no longer among the K closest to.
 **/
public class RestoreOperation extends Operation {
    private Configuration conf;
    private MessageServer server;
    private Space space;
    private Node local;
    private MultiMap localMap;
    private HashCalculator hasher;

    public RestoreOperation(Configuration conf, MessageServer server, Space space,
                            Node local, MultiMap localMap) {
        this.conf = conf;
        this.server = server;
        this.space = space;
        this.local = local;
        this.localMap = localMap;
        hasher = new HashCalculator(local, space, localMap);
    }

    /**
     * @return <code>null</code>
     * @throws I2PException     If any suboperation timed out
     * @throws IOException      If a network occurred
     **/
    public synchronized Object execute() throws IOException, I2PException {
        // Refresh buckets
        Operation refresh = new RefreshOperation(conf, server, space, local);
        refresh.execute();

        // Find nodes that are among the closest to mappings stored here
        // TODO: It is ineffective since HashCalculator also goes through all
        // mappings and looks at closestNodes. This should only be done once.

        Set<Node> nodes = new HashSet<Node>();
        Iterator<Identifier> it = localMap.keySet().iterator();
        while (it.hasNext()) {
            Identifier key = it.next();
            List<Node> closest = space.getClosestNodes(key);

            // No longer closest to mapping?
            if (!closest.contains(local)) {
                it.remove();
            } else {
                nodes.addAll(closest);
            }
        }
        // Do not send to self
        nodes.remove(local);

        // Calculate hashes and send HashMessage
        long now = System.currentTimeMillis();
        Iterator<Node> iter = nodes.iterator();
        while (iter.hasNext()) {
            Node node = iter.next();
            List<byte[]> hashes = hasher.logarithmicHashes(node, now);
            if (hashes.size() > 0) {
                Message mess = new HashMessage(local, now, hashes);
                server.send(mess, node.getDestination(), null);
            }
        }
        return null;
    }
}
