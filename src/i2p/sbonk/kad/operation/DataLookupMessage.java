package i2p.sbonk.kad.operation;

import java.io.DataInput;
import java.io.IOException;
import i2p.sbonk.kad.Identifier;
import i2p.sbonk.kad.Node;

public class DataLookupMessage extends LookupMessage {
    protected DataLookupMessage() {}

    public DataLookupMessage(Node origin, Identifier lookup) {
        super(origin, lookup);
    }

    public DataLookupMessage(DataInput in) throws IOException {
        super(in);
    }

    public byte code() {
        return MessageFactoryImpl.CODE_DATA_LOOKUP;
    }

    public String toString() {
        return "DataLookupMessage[origin="+origin+",lookup="+lookup+"]";
    }
}
