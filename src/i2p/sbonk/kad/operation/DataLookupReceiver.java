package i2p.sbonk.kad.operation;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.commons.collections.MultiMap;

import net.i2p.I2PException;
import i2p.sbonk.kad.Node;
import i2p.sbonk.kad.Identifier;
import i2p.sbonk.kad.Space;
import i2p.sbonk.kad.TimestampedValue;
import i2p.sbonk.kad.messaging.*;

/**
 * Responds to a DataLookupMessage by sending a DataMessage containing the
 * requested mapping <i>or</i> by sending a NodeReplyMessage containing
 * the <i>K</i> closest nodes to the request key known by this node.
 **/
public class DataLookupReceiver extends OriginReceiver {
    protected MultiMap localMap;

    public DataLookupReceiver(MessageServer server, Node local, Space space, 
    		MultiMap localMap) {
        super(server, local, space);
        this.localMap = localMap;
    }

    public void receive(Message incoming, int comm) throws IOException,
                                               UnknownMessageException, I2PException {
        DataLookupMessage mess = (DataLookupMessage) incoming;

        // Is data located here?
        Identifier key = mess.getLookupId();
        if (localMap.containsKey(key)) {
            super.receive(incoming, comm);

            // Return mapping for key
            Node origin = mess.getOrigin();
            @SuppressWarnings("unchecked")
			ArrayList<TimestampedValue> list = (ArrayList<TimestampedValue>) localMap.get(key);
            for (int i=0; i<list.size(); i++) {
            	Message reply = new DataMessage(local,key,list.get(i));
            	server.reply(comm, reply, origin.getDestination());
            }
        } else {
            // Return message with list of closest nodes
            Receiver recv = new NodeLookupReceiver(server, local, space);
            recv.receive(incoming, comm);
        }
    }
}
