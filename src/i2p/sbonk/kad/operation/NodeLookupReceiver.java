package i2p.sbonk.kad.operation;

import java.io.IOException;
import java.util.List;

import net.i2p.I2PException;
import i2p.sbonk.kad.Node;
import i2p.sbonk.kad.Space;
import i2p.sbonk.kad.messaging.*;

/**
 * Handles incoming LookupMessages by sending a NodeReplyMessage containing
 * the <i>K</i> closest nodes to the requested identifier.
 **/
public class NodeLookupReceiver extends OriginReceiver {
    public NodeLookupReceiver(MessageServer server, Node local, Space space) {
        super(server, local, space);
    }

    public void receive(Message incoming, int comm) throws IOException,
                                               UnknownMessageException, I2PException {
        super.receive(incoming, comm);

        LookupMessage mess = (LookupMessage) incoming;
        Node origin = mess.getOrigin();
        List<Node> nodes = space.getClosestNodes(mess.getLookupId());

        Message reply = new NodeReplyMessage(local, nodes);
        server.reply(comm, reply, origin.getDestination());
    }
}
