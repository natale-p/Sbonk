package i2p.sbonk.kad.operation;

import java.io.DataInput;
import java.io.IOException;
import i2p.sbonk.kad.Node;

public class ConnectMessage extends OriginMessage {
    protected ConnectMessage() {}

    public ConnectMessage(Node origin) {
        super(origin);
    }

    public ConnectMessage(DataInput in) throws IOException {
        super(in);
    }

    public byte code() {
        return MessageFactoryImpl.CODE_CONNECT;
    }

    public String toString() {
        return "ConnectMessage[origin="+origin+"]";
    }
}
