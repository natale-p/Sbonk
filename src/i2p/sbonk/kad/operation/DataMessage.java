package i2p.sbonk.kad.operation;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import i2p.sbonk.kad.DNSValue;
import i2p.sbonk.kad.Identifier;
import i2p.sbonk.kad.Node;
import i2p.sbonk.kad.TimestampedValue;

public class DataMessage extends OriginMessage {
    protected Identifier key;
    protected TimestampedValue value;

    protected DataMessage() {}

    public DataMessage(Node origin, Identifier key, TimestampedValue value) {
        super(origin);
        this.key = key;
        this.value = value;
    }

    public DataMessage(DataInput in) throws IOException {
        fromStream(in);
    }

    public void fromStream(DataInput in) throws IOException {
        super.fromStream(in);
        key = new Identifier(in);
        byte type = in.readByte();
        switch (type) {
        case TimestampedValue.T_DNS:
        	value = new DNSValue(in);
        	break;
        default:
        	value = null;
        }
    }

    public void toStream(DataOutput out) throws IOException {
        super.toStream(out);
        key.toStream(out);
        value.toStream(out);
    }

    public Identifier getKey() {
        return key;
    }

    public TimestampedValue getValue() {
        return value;
    }

    public byte code() {
        return MessageFactoryImpl.CODE_DATA;
    }

    public String toString() {
        return "DataMessage[origin="+origin+",key="+key+",value="+value+"]";
    }
}
