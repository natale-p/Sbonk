package i2p.sbonk.kad.operation;

import java.io.DataInput;
import java.io.IOException;
import i2p.sbonk.kad.Identifier;
import i2p.sbonk.kad.Node;

public class NodeLookupMessage extends LookupMessage {
    protected NodeLookupMessage() {}

    public NodeLookupMessage(Node origin, Identifier lookup) {
        super(origin, lookup);
    }

    public NodeLookupMessage(DataInput in) throws IOException {
        super(in);
    }

    public byte code() {
        return MessageFactoryImpl.CODE_NODE_LOOKUP;
    }

    public String toString() {
        return "NodeLookupMessage[origin="+origin+",lookup="+lookup+"]";
    }
}
