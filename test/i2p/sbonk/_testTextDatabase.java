package i2p.sbonk;

import i2p.sbonk.db.TextDatabase;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.LinkedList;

import net.i2p.data.DataFormatException;
import net.i2p.data.Destination;

import junit.framework.TestCase;

public class _testTextDatabase extends TestCase {
	private static Destination localDest;
	
	public static void main(String args[]) {
		
		Configuration.pluginPath = ".";
		File f = new File("plugins/sbonk/db");
		File d = new File("plugins/sbonk/net");
		f.mkdir();
		d.mkdir();
		

		LocalSbonker sbk = LocalSbonker.getInstance();
		try {
			sbk.setName("tst");
			sbk.savePersistently();
			localDest = sbk.getDestination();
			
			System.out.println("Local destination: " + localDest.toBase64());
			
			assertEquals(sbk.getName(), "tst");
			File g = new File("plugins/sbonk/db/tst/main_profile.dat");
			assert(g.exists());
			
			Post pst = new Post();
			pst.setText("TEEEST");
			sbk.addPost(pst);
			
			assertEquals(sbk.getPostCount(), 1);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
		org.junit.runner.JUnitCore.main(_testTextDatabase.class.getName());
	}

	public void testLoadRemoteSbonker() throws DataFormatException {
		try {
			FileInputStream in = new FileInputStream(Configuration.databasePath + "tst/main_profile.dat");
			RemoteSbonker sbk1 = new RemoteSbonker(in);
			assertEquals(sbk1.getName(), "tst");
			assertEquals(sbk1.getDestination(), localDest);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			assertEquals(0,1);
		} catch (IOException e) {
			e.printStackTrace();
			assertEquals(0,1);
		}
	}
	
	public void testListOfAllSbonkers() {
		TextDatabase db = TextDatabase.getInstance();
		LinkedList<Sbonker> list = db.getAllSbonkers();
		
		assertEquals(list.size(), 1);
	}
	
	public void testLocalPostList() {
		LocalSbonker sbk = LocalSbonker.getInstance();

		LinkedList<Post> list = sbk.getPosts();

		assertEquals(list.size(), 1);
		assertEquals(sbk.getPostCount(), 1);
		
		assertEquals(list.get(0).getText(), "TEEEST");
	}
}
