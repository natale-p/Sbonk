package i2p.sbonk;

import i2p.sbonk.kad.Identifier;
import i2p.sbonk.kad.Kademlia;
import i2p.sbonk.kad.Node;
import i2p.sbonk.kad.TimestampedValue;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

import junit.framework.TestCase;
import net.i2p.I2PException;
import net.i2p.client.I2PClient;
import net.i2p.client.I2PClientFactory;
import net.i2p.client.I2PSession;

public class _testKademlia extends TestCase {

	private I2PSession session;
	
	private void waitT(int time) {
		long timeNow = System.currentTimeMillis();
		while((timeNow + time)>System.currentTimeMillis());
	}
	
	public static void main(String args[]) {
	      org.junit.runner.JUnitCore.main(_testKademlia.class.getName());
	}
	
	public void setUp() {
		
	}
	
	public void tearDown() {
	}
	
	public Kademlia getKad() throws IOException, I2PException {
		I2PClient client = I2PClientFactory.createClient();
		PipedInputStream in = new PipedInputStream();
		PipedOutputStream out = new PipedOutputStream();
		out.connect(in);
		
		client.createDestination(out);
		Properties prop = new Properties();
		
		session = client.createSession(in, prop);
		
		in.close();
		out.close();
		
		Kademlia net = null;
		try {
			net = new Kademlia(Identifier.randomIdentifier(), session);
		} catch (IOException e) {
			System.out.println("Creating kad: " + e.getLocalizedMessage());
		} catch (I2PException e) {
			System.out.println("Creating kad: " + e.getLocalizedMessage());
		}
		
		assert((net != null));
		
		return net;
	}
	
	public static void orderNodes(List<Node> nodes, Identifier id) {
        Collections.sort(nodes, new Node.DistanceComparator(id));
    }
	
	public void testSimpleConnect() throws IOException, I2PException {
		Kademlia net1 = null, net2 = null;
		net1 = getKad();
		net2 = getKad();
		assert((net1 != null) && (net2 != null));

		System.out.println("net1 è " + net1.getLocalDestination().toBase64().substring(0,4));
		System.out.println("net2 è " + net2.getLocalDestination().toBase64().substring(0,4));
		
		net2.connect(net1.getLocalDestination());
		
		List<Node> l1 = net1.internalGetSpace().getAll();
        List<Node> l2 = net2.internalGetSpace().getAll();
        
        orderNodes(l1, new Identifier());
        orderNodes(l2, new Identifier());
        
        assertEquals(l1, l2);
	}
	
	@SuppressWarnings("unchecked")
	public void testSimplePutGet() throws IOException, I2PException {
		Kademlia net1 = null, net2 = null;
		Identifier id = new Identifier("mmm:uIsP");
		
		net1 = getKad();
		net2 = getKad();
		
		assert((net1 != null) && (net2 != null));
		
		net2.connect(net1.getLocalDestination());
		net2.put(id, "uIsP1", (byte) 0x1);
		
		waitT(1000);
		
		assert(net1.internalGetMap().values().size() == net2.internalGetMap().values().size());
		ArrayList<TimestampedValue> list = (ArrayList<TimestampedValue>) net1.get(new Identifier("Test"));
		
		assert(list != null);
		assert(list.size() == 1);
		assert(list.get(0).getObject().equals("uIsP1"));

		net2.put(id, "uIsP2", (byte) 0x1);
		net2.put(id, "uIsP3", (byte) 0x1);
		try {
			net2.put(id, "4", (byte) 0x1);
		} catch (IOException e) {
			// All ok, key is invalid
		}
		try {
			net2.put(id, "sadsaddsa", (byte) 0x1);
		} catch (IOException e) {
			// All ok, key is invalid
		}
		try {
			net2.put(id, "uIs", (byte) 0x1);
		} catch (IOException e) {
			// All ok, key is invalid
		}

		waitT(1000);
		
		list = (ArrayList<TimestampedValue>) net1.get(new Identifier("Test"));
		
		assert(list != null);
		assert(list.size() == 6);
		assert(list.get(0).getObject().equals("uIsP1"));
		assert(list.get(2).getObject().equals("uIsP3"));
	}
}
