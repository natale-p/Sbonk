#!/bin/bash

I2P="/opt/i2p"
JUNIT="/usr/share/java/junit.jar"

CLASSPATH="$I2P/lib/i2p.jar:$I2P/lib/mstreaming.jar:$I2P/lib/streaming.jar:$JUNIT:../ant_build/obj:./ant_build/obj:../lib/commons-collections-3.2.1.jar"

java -classpath $CLASSPATH i2p.sbonk._testKademlia
