<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="i2p.sbonk.LocalSbonker" %>

<%
	out.write("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
	out.write("<response>");
    
	LocalSbonker localSbk = LocalSbonker.getInstance();
	out.write("<netname>" + localSbk.getNetName()() + "</netname>");
	out.write("<nickname>" + localSbk.getName() + "</nickname>");
	out.write("<destination>"+ localSbk.getDestination().toBase64() + "</destination>");
	
    out.write("</response>");
%>