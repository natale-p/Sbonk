<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="i2p.sbonk.LocalSbonker"%>

<%
	out.write("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
	out.write("<response>");
    if ( request.getParameter("publicname") != null ) {
    	String[] paramValues = request.getParameterValues("publicname");
    	if (paramValues.length == 1) {
    		try {
    			LocalSbonker sbk = LocalSbonker.getInstance();
    			sbk.setName(paramValues[0]);
    			out.write("<success>Name saved</success>");
    		} catch (Exception e) {
    			out.write("<error>"+e.getMessage()+"</error>");
    		}
    	}
    } else {
    	out.write("<error>No name parameter on the request.</error>");
    }
    
    out.write("</response>");
%>