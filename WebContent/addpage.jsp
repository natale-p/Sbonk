		<div data-role="page" id="add" data-theme="b" data-title="Sbonk! Add things">
	
		<div data-role="header" data-theme="b">
			<a href="#main"  data-role="button" data-inline="true" data-icon="back">Back to the Home</a>
			<h2>Sbonk! Add things</h2>
			<a href="#connect" data-icon="gear" data-theme="b" class="connectBtn">Connect</a>
		</div><!-- /header -->
	
		<div data-role="content" data-theme="b">
				<img class="intro" src="css/images/logo.png" />
				<h2 class="intro">What things could you add?</h2>
				<p class="intro">A <b>sbonk</b>, which is the text you want to spread anonymously to the world.</p>
				<p class="intro">A <b>sbonker</b>, which is a person that you want to follow, by reading his/her sbonks.</p>

				<div data-role="collapsible" data-collapsed="true">
					<h3 class="intro">Where these things will be saved? There are risks?</h3>
					<p class="intro">On a local database, saved on your's I2P home directory. If other people
					gain access to your account on your computer, they could easily spoof you, editing your sbonks
					or sbonking something bad. For other users on your computer, check your I2P home directory
					permissions and give ONLY to your user the read and write permissions.</p>
				</div>
				
				<a class="action" data-role="button" href="#insertSbonker" data-rel="dialog" data-inline="true" data-theme="a">Add Sbonker</a>
				<a class="action" data-role="button" href="#insertSbonk" data-rel="dialog" data-inline="true" data-theme="a">Add Sbonk</a>
		</div><!-- /content -->
		
		<%@ include file="footer.jsp" %> 
	</div><!-- /page -->