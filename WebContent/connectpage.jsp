
	<div data-role="page" id="connect" data-theme="b" data-title="Sbonk! Connect to a bootstrap node">
	
		<div data-role="header" data-theme="d" data-position="inline">
			<h1>Manually connect to a node</h1>
		</div>

		<form>
			<fieldset>
				<div data-role="fieldcontain">
					<label for="bootstrapNode">
						<b>You aren't connected</b> to the Sbonk's network.<br />
						Insert a valid destination to a known bootstrap node in order to
						connect to the Sbonk's network.
					</label>
   					<input type="text" name="bootstrapNode" id="bootstrapNode" value=""  />
				</div>
				<button id="btnSubmitBootstrap" type="submit" data-theme="a" name="submit" value="submit-value">Save and try to connect</button>
			</fieldset>
		</form>
	</div>