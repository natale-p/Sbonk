<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="i2p.sbonk.net.I2PNetwork"%>

<%
	out.write("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
	out.write("<response>");
    if ( request.getParameter("bootstrapNode") != null ) {
    	String[] paramValues = request.getParameterValues("bootstrapNode");
    	if (paramValues.length == 1) {
    		try {
    			I2PNetwork net = I2PNetwork.getInstance();
    			net.connectTo(paramValues[0]);
    			out.write("<success>Connection done. You're on the road :-)</success>");
    		} catch (Exception e) {
    			out.write("<error>"+e.getMessage()+"</error>");
    		}
    	}
    } else {
    	out.write("<error>No destination parameter on the request.</error>");
    }
    
    out.write("</response>");
%>