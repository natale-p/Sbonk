
	<div data-role="page" id="insertSbonker" data-theme="b" data-title="Sbonk! Insert a Sbonker to follow">
	
		<div data-role="header" data-theme="d" data-position="inline">
			<h1>Insert a Sbonker to follow</h1>
		</div>

		<form>
			<fieldset>
				<div data-role="fieldcontain">
					<label for="sbonkersname">
						Insert the sbonker's name (plus the four digits that represent the destination hash,)
						prefixed by ":" without quotes) or sbonker's destination, if you already know it:
					</label>
   					<input type="text" name="sbonkersname" id="sbonkersname" value=""  />
   					</div>
				<button id="btnSubmitSbonker" type="submit" data-theme="a" name="submit" value="submit-value">Add</button>
			</fieldset>
		</form>
	</div>
				
	</div><!-- /page -->