<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="i2p.sbonk.RemoteSbonker"%>
<%@page import="java.io.IOException "%>
<%@page import="net.i2p.I2PException "%>

<%
	out.write("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
	out.write("<response>");
    if ( request.getParameter("sbonkersname") != null ) {
    	String[] paramValues = request.getParameterValues("sbonkersname");
    	if (paramValues.length == 1) {
    		if (paramValues[0].contains(":")) {
    			try {
    				RemoteSbonker sbk = new RemoteSbonker(paramValues[0]);
    				sbk.savePersistently();
    				out.write("<success>Sbonker saved successfully</success>");
    			} catch (IOException e) {
    				out.write("<error>"+e.getMessage()+"</error>");
    			} catch (I2PException e) {
    				out.write("<error>"+e.getMessage()+"</error>");
    			}
    		} else { 
    			out.write("TODO: Add destination here");
    		}
    	} else {
    		out.write("<error>Too many parameters</error>");
    	}
    } else {
    	out.write("<error>No sbonker parameter.</error>");
    }
    
    out.write("</response>");
%>