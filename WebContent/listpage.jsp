		<div data-role="page" id="list" data-theme="b" data-title="Sbonk! List things">
	
		<div data-role="header" data-theme="b">
			<a href="#main"  data-role="button" data-inline="true" data-icon="back">Back to the Home</a>
			<h2>Sbonk! List things</h2>
			<a href="#connect" data-icon="gear" data-theme="b" class="connectBtn">Connect</a>
		</div><!-- /header -->
	
		<div data-role="content" data-theme="b">
				<img class="intro" src="css/images/logo.png" />
				<h2 class="intro">What I could see here?</h2>
				<p class="intro">All <b>your sbonker</b>, and selecting a sbonker by clicking on his/her nickname, all <b>his/her sbonks</b>.</p>
				<p class="intro">All <b>your sbonks</b>.</p>

				<div data-role="collapsible" data-collapsed="true">
					<h3 class="intro">Are these lists public?</h3>
					<p class="intro">Your sbonker's list is <b>private</b>. It means that only you know it.</p>
					<p class="intro">At this time, all your sbonks are <b>public</b>. Everyone that knows your netname could
					connect to you and download your sbonk's list. <b>Note that</b> this may change in future, when
					circles of friends <i>will be implemented.</i> (allowing sbonks to be downloaded only by selected people) </p>
				</div>
				<div data-role="collapsible" data-collapsed="true">
					<h3 class="intro">Could someone else see or intercept what I am seeing?</h3>
					<p class="intro">All connections are over I2P network. By design of I2P,
					other people <i>can see that you're on the I2P network</i>, but <b>nobody can see</b> what you're
					doing over this network, or which node are you contacting.</p>
					<p class="intro">Any messages are sent or received by some central authority: all
					sbonks are <i>encrypted and sent directly peer-to-peer.</i></p>
				</div>
				
				<a id="btnListSbonker" class="action" href="#" data-role="button" data-inline="true" data-theme="a">My Sbonker's List</a>
				<a id="btnListSbonk"   class="action" href="#" data-role="button" data-inline="true" data-theme="a">My Sbonk's List</a>
		</div><!-- /content -->
		
		<div data-role="content" data-theme="b">
			<ul id="listSbonker" data-role="listview" data-theme="g">
			</ul>
		</div><!-- /content -->
		<%@ include file="footer.jsp" %> 
		
	</div><!-- /page -->