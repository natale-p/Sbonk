<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="i2p.sbonk.LocalSbonker"%>
<%@page import="i2p.sbonk.Post"%>

<%
	out.write("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
	out.write("<response>");
    if ( request.getParameter("sbonk") != null ) {
    	Post post = new Post();
    	String[] paramValues = request.getParameterValues("sbonk");
    	if (paramValues.length == 1) {
			post.setText(paramValues[0]);
			try {
				LocalSbonker sbk = LocalSbonker.getInstance();
				sbk.addPost(post);
				out.write("<success>Sbonk saved successfully</success>");
			} catch (Exception e) {
				out.write("<error>Can't save the sbonk for " + e.getMessage());
			}
    	} else {
    		out.write("<error>Too many post parameters</error>");
    	}
    } else {
    	out.write("<error>No post parameter.</error>");
    }
    
    out.write("</response>");
%>