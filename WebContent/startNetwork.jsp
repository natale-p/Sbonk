<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="i2p.sbonk.Configuration"%>
<%@page import="java.io.IOException" 
		import="i2p.sbonk.net.I2PNetwork"
		import="i2p.sbonk.LocalSbonker" 

		import="java.io.BufferedReader"
		import="java.io.BufferedWriter"
		import="java.io.DataInputStream"
		import="java.io.FileInputStream"
		import="java.io.FileNotFoundException"
		import="java.io.IOException"
		import="java.io.InputStreamReader"

		import="net.i2p.I2PException"
		import="net.i2p.data.DataFormatException"
		import="net.i2p.data.Destination"
%>


<%
	out.write("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
	out.write("<response>");
	
	// Create first a localSbk, or network will be unitialized
	LocalSbonker localSbk = LocalSbonker.getInstance();
	out.write("<netname>" + localSbk.getNetName() + "</netname>");
	out.write("<nickname>" + localSbk.getName() + "</nickname>");
	out.write("<destination>"+ localSbk.getDestination().toBase64() + "</destination>");
	
	I2PNetwork net = I2PNetwork.getInstance();
	
	if (net.isConnected()) {
		out.write("<success>Already connected</success>");
	} else {
		Destination btDest = null;
		
		try {
			FileInputStream fstream;
			fstream = new FileInputStream(Configuration.bootstrapNodeFile);
			DataInputStream in = new DataInputStream(fstream);
		    BufferedReader br = new BufferedReader(new InputStreamReader(in));
		    String bootstrapDest = br.readLine();
		    
		    try {
		    	btDest = new Destination();
				btDest.fromBase64(bootstrapDest);
				out.write("<success>Read the default boostrapping destination</success>");
			} catch (DataFormatException e) {
				btDest = null;
				out.write("<error>Malformed bootstrap destination. Modify the file " + Configuration.bootstrapNodeFile);
				out.write(" and retry, or connect by hand to a know node.</error>");
			}
		} catch (FileNotFoundException e) {
			out.write("<error>File with default boostrapping nodes not found.");
			out.write("To join the network, you must connect by hand to a know node.</error>");
		} catch (IOException e) {
			out.write("<error>File with default boostrapping nodes isn't readables.");
			out.write("To join the network, you must connect by hand to a know node.</error>");
		}
		
		if (btDest != null) {
			try {
				net.connectTo(btDest);
				out.write("<success>Connected to the network</success>");
			} catch (Exception e) {
				out.write("<error>You aren't connected to the network ("+e.getMessage()+"). Please do it manually</error>");
			}
		} else {
			out.write("<error>You aren't connected to the network (no default bootstrap destination). Please do it manually</error>");
		}
	}
    out.write("</response>");
%>