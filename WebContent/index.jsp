<%@ page language="java" contentType="text/html; charset=UTF-8" 
    pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html> 
	<head> 
	<title>Sbonk!</title> 
	
	<meta name="viewport" content="width=device-width, initial-scale=1"> 

	<link rel="stylesheet" href="css/jquery.mobile-1.0b2.min.css" />
	<link rel="stylesheet" href="css/index.css" />
	<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script>
	<script type="text/javascript" src="js/jquery.mobile-1.0b2.min.js"></script>
	
	<script type="text/javascript" src="js/index.js"></script>
</head> 

<body> 
	     <%@ include file="mainpage.jsp" %> 
		 <%@ include file="addpage.jsp"  %>
		 <%@ include file="listpage.jsp"  %>
		 <%@ include file="insertSbonkpage.jsp"  %>
		 <%@ include file="insertSbonkerpage.jsp"  %>
		 <%@ include file="insertNamepage.jsp"  %>
		 <%@ include file="connectpage.jsp"  %>
</body>
</html>