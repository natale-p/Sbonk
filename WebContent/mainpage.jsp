	<div data-role="page" id="main" data-theme="b" data-title="Sbonk! Home">
	
		<div  data-role="header" data-theme="b">
			<h2>Sbonk! Home</h2>
			<a href="#connect" data-icon="gear" data-theme="b" class="connectBtn">Connect</a>
		</div><!-- /header -->
	
		<div data-role="content" data-theme="b">
				<img class="intro" src="css/images/logo.png" />
				<h2 class="intro">About</h2>
				<p class="intro">This is Sbonk!, an anonymous, distribuited micro-blogging platform.</p>
				<p class="intro">You could follow a lot of sbonker, or sbonk a message yourself.</p>

				<a class="intro" href="#add"  data-role="button" data-inline="true" data-icon="plus">Add</a>
				<a class="intro" href="#list" data-inline="true" data-role="button" data-icon="grid">List</a>
				<a class="intro" href="#" data-inline="true" data-role="button" data-icon="gear">Configuration</a>
				<a class="intro" href="#" data-inline="true" data-role="button" data-icon="info">Info</a>
		</div><!-- /content -->
		<%@ include file="footer.jsp" %> 
	</div><!-- /page -->
