
function sbonk_submitted(xml)
{
	$(xml).find("error").each(function() {showError($(this)); });
	$(xml).find("success").each(function() {showStatus($(this)); });
	
	$.mobile.changePage( $("#main"), { transition: "slidedown"} );
}

function name_submitted(xml)
{
	$(xml).find("error").each(function() {showError($(this)); });
	$(xml).find("success").each(function() {showStatus($(this)); });
	
	$.mobile.changePage( $("#main"), { transition: "slidedown"} );
}

function sbonker_submitted(xml)
{
	$(xml).find("error").each(function() {showError($(this)); });
	$(xml).find("success").each(function() {showStatus($(this)); });
	
	$.mobile.changePage( $("#add"), { transition: "slidedown"} );
}

function sbonkerlist_arrived(xml)
{
	$("#listSbonker").html("");
	$(xml).find("sbonker").each(function() {
		var name = $(this).attr("name");
		var count = $(this).attr("count");
		
		$("#listSbonker").append("<li><a href=\"#\">"+name+
				"<div class=\"ui-li-count\">"+count+"</div></a></li>");
	});
	
	$('ul').listview('refresh');
}

function sbonklist_arrived(xml)
{
}

function bootstrap_submitted(xml)
{
	$(".connectBtn").hide();
	
	$(xml).find("error").each(function() {showError($(this)); 
	$(".connectBtn").show();});
	$(xml).find("success").each(function() {showStatus($(this)); });

	$.mobile.changePage( $("#main"), { transition: "slidedown"} );
}
function btnSubmitBootstrap_clicked()
{
	$(".footer").html("");
	var node = $("#bootstrapNode").val();
    $.ajax({  
        type: "POST",  
        url: "connect.jsp",  
        data: "bootstrapNode="+node,
        success: bootstrap_submitted
      });
    return false;
}

function btnSubmitSbonker_clicked()
{
	$(".footer").html("");
	var sbonkersname = $("#sbonkersname").val();
    $.ajax({  
        type: "POST",  
        url: "insertSbonker.jsp",  
        data: "sbonkersname="+sbonkersname,
        success: sbonker_submitted
      });
    return false;
}


function btnSubmitSbonk_clicked()
{
	$(".footer").html("");
	var sbonkText = $("#txtSbonk").val();
    $.ajax({  
        type: "POST",  
        url: "insertSbonk.jsp",  
        data: "sbonk="+sbonkText,
        success: sbonk_submitted
      });
    return false;
}

function btnSubmitName_clicked()
{
	$(".footer").html("");
	var name = $("#publicName").val();
    $.ajax({  
        type: "POST",  
        url: "insertName.jsp",  
        data: "publicname="+name,
        success: name_submitted
      });
    return false;
}

function btnListSbonk_clicked()
{
	$(".footer").html("");
    $.ajax({  
        type: "POST",  
        url: "getSbonkList.jsp",  
        data: "",
        success: sbonklist_arrived
      });
    return false;
}

function btnListSbonker_clicked()
{
	$(".footer").html("");
    $.ajax({  
        type: "POST",  
        url: "getSbonkerList.jsp",  
        data: "",
        success: sbonkerlist_arrived
      });
    
    return false;
}
function showError(error)
{
	$(".footer").append("<div class=\"errormsg\">"+error[0].textContent+"</div>");
}

function showStatus(status)
{
	$(".footer").append("<div class=\"statusmsg\">"+status[0].textContent+"</div>");
}

function names_arrived(xml)
{
	$(xml).find("nickname").each(function() { 
		if ($(this)[0].textContent == "") {
			$.mobile.changePage( $("#insertName"), { transition: "slideup"} );	
		} else {
			$(".nickname").text("Your nickname: " + $(this)[0].textContent);
		}
	});
	
	$(xml).find("netname").each(function() { $(".netname").text("Your netname: " + $(this)[0].textContent); });
}

function network_started(xml)
{
	$(xml).find("error").each(function() {showError($(this)); $(".connectBtn").show();});
	$(xml).find("success").each(function() {showStatus($(this)); });
	
    $.ajax({  
        type: "POST",  
        url: "getNames.jsp",  
        data: "",
        success: names_arrived
    });
}

function startController()
{
    $.ajax({  
        type: "POST",  
        url: "startNetwork.jsp",  
        data: "start=true",
        success: network_started
    });  
}

function start() {
	$(".connectBtn").hide();
	startController();
	$("#btnSubmitSbonk").click(btnSubmitSbonk_clicked);
	$("#btnSubmitName").click(btnSubmitName_clicked);
	$("#btnSubmitBootstrap").click(btnSubmitBootstrap_clicked);
	$("#btnSubmitSbonker").click(btnSubmitSbonker_clicked);
	$("#btnListSbonk").click(btnListSbonk_clicked);
	$("#btnListSbonker").click(btnListSbonker_clicked);
}

$('#main').live('pagecreate',function(event){
	  start();	
});