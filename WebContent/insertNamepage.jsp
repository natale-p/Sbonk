	
	<div data-role="page" id="insertName" data-theme="b" data-title="Sbonk! Insert your name">
	
		<div data-role="header" data-theme="d" data-position="inline">
			<h1>Before continuing to Sbonk!: Insert your name</h1>
		</div>

		<form>
			<fieldset>
				<div data-role="fieldcontain">
					<label for="publicName">
						<b>Welcome on Sbonk!</b><br />
						This seems your first time here, because your name is missing =) <br />
						Other can reach you only if they know <i>your name</i>. 
						To keep your identity private, <b>don't use</b> your real name.<br />
						It will be associated to four random digits, to prevent situations were people have the same name.<br />
						Don't worry, you could change it when you want.<br />
					</label>
   					<input type="text" name="publicName" id="publicName" value=""  />
				</div>
				<button id="btnSubmitName" type="submit" data-theme="a" name="submit" value="submit-value">Save and continue</button>
			</fieldset>
		</form>
	</div>
				
	</div><!-- /page -->